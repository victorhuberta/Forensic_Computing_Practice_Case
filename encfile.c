/* Author: Victor Huberta
 * Dumb encryption scheme for a project of mine.
 * The messed up naming is done on purpose. */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int main(int g, char **v)
{
    char *x, *e;
    int f, i = 0, k = 0, r = 0, n = 0;
    char b[2] = {0};
    if (g != 3) return 1;
    x = v[1]; e = v[2];
    f = open(x, O_RDWR);
    if (f == -1) return 1;
    for (i = 0; e[i] != '\0'; ++i, ++n); i = 0;
    while (1) {
        if (lseek(f, i, SEEK_SET) == -1) return 1;
        if ((r = read(f, b, 1)) == -1) return 1;
        if (!r) break;
        b[0] = ((b[0]^e[k])>>n)|(b[0]^e[k])<<((sizeof(b[0])*8)-n);
        if (++k >= n) k = 0;
        if (lseek(f, i++, SEEK_SET) == -1) return 1;
        if (write(f, b, 1) == -1) return 1;
    }
    close(f);
    return 0;
}
