def xor_str(s, key):
    l = [ord(c) for c in s]
    res = [i ^ key for i in l]
    return ''.join([chr(i) for i in res])
