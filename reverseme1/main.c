#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char answer[256] = {0};
    char pass[] = "ZK^^OXDY"; // patterns
    printf("What is love? ");
    scanf("%[^\n]", answer);
    getchar();
    if (strcmp(answer, "Baby, don't hurt me") != 0) {
        printf("You are incorrect. Please DON'T try again.\n");
        return 0;
    }
    printf("What is the meaning of life? ");
    scanf("%[^\n]", answer);
    getchar();
    if (strcmp(answer, "#DICKSOUT4HARAMBE") == 0) {
        printf("You are correct.\n");
        for (int i = 0; i < 8; ++i) {
            pass[i] ^= 42;
        }
    } else {
        printf("You are incorrect. Please DON'T try again.\n");
    }
    return 0;
}
