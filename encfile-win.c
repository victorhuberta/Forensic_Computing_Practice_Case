/* Author: Victor Huberta
 * Dumb encryption scheme for a project of mine.
 * The messed up naming is done on purpose. */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int main(int g, char **v)
{
    char *x, *e;
    int f, i = 0, k = 0, r = 0, n = 0;
    char b[2] = {0};
    if (g != 3) return 1;
    x = v[1]; e = v[2];
    f = CreateFile(x,GENERIC_READ|GENERIC_WRITE,0,0,3,128,NULL);
    if (f == INVALID_HANDLE_VALUE) return 1;
    for (i = 0; e[i] != '\0'; ++i, ++n); i = 0;
    while (1) {
        if (SetFilePointer(f,i,0,0) == INVALID_SET_FILE_POINTER) return 1;
        if ((r = ReadFile(f, b, 1, &z, 0)) == 0) return 1;
        if (z==0) break;
        b[0] = ((b[0]^e[k])>>n)|(b[0]^e[k])<<((sizeof(b[0])*8)-n);
        if (++k >= n) k = 0;
        if (SetFilePointer(f,i++,0,0) == INVALID_SET_FILE_POINTER) return 1;
        if (WriteFile(f, b, 1, &z, 0) == 0) return 1;
    }
    CloseHandle(f);
    return 0;
}
