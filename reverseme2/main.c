#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void doSomething()
{
    doSomething();
}

int main(int argc, char **argv)
{
    HANDLE f;
    DWORD z = 0;
    char answer[256] = {0};
    char pass[] = "4%26#>89"; // creation
    printf("What is the inverse of 9475849 under mod 11? ");
    scanf("%[^\n]", answer);
    getchar();
    if (strcmp(answer, "42") != 0) {
        printf("You are incorrect. Please DON'T try again.\n");
        return 0;
    }
    printf("What does no one try to eat a clock? ");
    scanf("%[^\n]", answer);
    getchar();
    if (strcmp(answer, "Because it's very time consuming.") == 0) {
        printf("You are correct.\n");
        doSomething();
        f = CreateFile("genesis",GENERIC_WRITE,0,0,2,2,NULL);
        if (f == INVALID_HANDLE_VALUE) return 1;
        for (int i = 0; i < 8; ++i) {
            pass[i] ^= 87;
            if (WriteFile(f, pass + i, 1, &z, 0) == 0) return 1;
            pass[i] ^= 87;
        }
        CloseHandle(f);
        DeleteFile("genesis");
    } else {
        printf("You are incorrect. Please DON'T try again.\n");
    }
    return 0;
}
