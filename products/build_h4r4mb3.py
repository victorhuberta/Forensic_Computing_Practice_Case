#!/usr/bin/env python3

import sys
import subprocess

def main(args):
    action = args['action']
    fname = args['fname']
    if action == 'obfuscate':
        obfuscate(fname)
    elif action == 'deobfuscate':
        deobfuscate(fname)
    elif action == 'compile':
        compile_with_gcc(fname)
    else:
        print('Unknown action. Abort.')
        sys.exit(1)


def obfuscate(fname):
    with open(fname, 'r') as f:
        lines = [line for line in f]
        obfuscated = switch_lines(lines)

    obfuscated = ''.join(reversed(obfuscated))
    with open(fname, 'w') as f:
        f.write(obfuscated)


def switch_lines(lines):
    result = ''
    for i in range(0, len(lines), 2):
        result += lines[i+1]
        result += lines[i]
        if len(lines) % 2 == 1:
            if i + 3 >= len(lines):
                result += lines[i+2]
                break
    return result


def deobfuscate(fname):
    with open(fname, 'r') as f:
        content = ''.join(reversed(f.read()))
        lines = [line + '\n' for line in content.split('\n')]
        clean_text = switch_lines(lines)

    with open(fname, 'w') as f:
        f.write(clean_text)


def compile_with_gcc(fname):
    subprocess.call(['gcc', fname, '-o', fname.split('.c')[0],
        '-z', 'execstack'])


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: ./{} [obfuscate/deobfuscate/compile] filename')
        sys.exit(1)

    action = sys.argv[1]
    fname = sys.argv[2]
    main({'action': action, 'fname': fname})
